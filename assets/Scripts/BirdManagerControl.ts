import BirdControl from "./BirdControl";


const {ccclass, property} = cc._decorator;

@ccclass
export default class BirdManagerControl extends cc.Component {

    // 小鸟预设体
    @property(cc.Prefab)
    birdPrefab: cc.Prefab;

    // 1s出现一只小鸟
    @property
    time: number = 1;

    // 分数
    @property
    score: number = 0;

    // 分数标签
    @property(cc.Label)
    scoreLabel: cc.Label;

    // 返回视图
    @property(cc.Node)
    back_view: cc.Node;

    start() {
        // 每隔1s创建一只小鸟
        this.node.runAction(cc.repeatForever(cc.sequence(cc.delayTime(this.time), cc.callFunc(() => {
            // 创建小鸟
            let bird = cc.instantiate(this.birdPrefab);
            // 设置父物体
            bird.setParent(this.node);
            // 设置小鸟位置
            bird.y = this.node.y;
            bird.x = Math.random() * 220 - 110;
            // 飞
            // bird.getComponent(BirdControl).fly();
            // 加分回调
            bird.getComponent(BirdControl).addScoreCallback = () => {
                this.score += 100;
                // console.log("分数:" + this.score);
                this.scoreLabel.string = this.score + "";
            };
            // 游戏结束回调
            bird.getComponent(BirdControl).dieCallback = () => {
                this.node.destroyAllChildren();
                this.node.stopAllActions();
                console.log("死亡");
                this.back_view.active = true;
            };
        }))));

    }

    backView() {
        cc.director.loadScene("start");
    }
}
